import { PubSub } from 'graphql-subscriptions';

export const pubsub = new PubSub();

export default {
  Query: {
    posts: async (parent, args, { models }) => {
      const Posts = await models.Post.find({});
      return Posts;
    },
    getGreeting: (parent, args, models) => {
      const greeting = 'Hello from the server!';
      return greeting;
    }
  },
  Mutation: {
    createPost: async (parent, { title, desc, author }, { models }) => {
      console.log('createPost', title);
      const Post = await models.Post.findOne({ title });

      if (Post) {
        throw new Error('Please provide a unique title.');        
      }
      
      // create a new post
      const newPost = new models.Post({
        title,
        desc,
        author
      });

      console.log('newPost', newPost);

      // save the post
      try {
        await newPost.save(function(err, user) {
          if (err) {
              console.log(err);
          }
        });
      } catch (e) {
        throw new Error('Cannot Save Post!!!');
      }

      return true;
    },
    removePosts: async (parent, args, { models }) => {
      // delete the post
      try {
        await models.Post.remove({}, function(err) {
          console.log('Posts removed.')
        });
      } catch (e) {
        throw new Error('Cannot Remove Posts!!!');
      }

      return true;
    },
  },
};